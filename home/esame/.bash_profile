#export http_proxy="http://proxy2.edu-al.unipmn.it:3128"
#export https_proxy="http://proxy2.edu-al.unipmn.it:3128"

# Source global definitions
if [ -f /etc/bashrc ]; then
        . /etc/bashrc
fi

# Well, this is ugly...		[bta, 20100722
#PS1='[\u@\h \w <\t>]$ '

# Environment for Java
export PATH="/opt/java/bin:/opt/netbeans/bin/:$PATH"
export JAVA_HOME="/opt/java"

# Environment for Android development
export PATH="$PATH:/opt/android-sdk-linux/build-tools/latest/:/opt/android-sdk-linux/platform-tools/:/opt/android-sdk-linux/tools/:/opt/apache-ant/bin/"
export ANDROID_HOME="/opt/android-sdk-linux/"
