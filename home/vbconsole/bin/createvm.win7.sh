#!/bin/bash

NAME="Windows 7"
OSTYPE="Windows7_64"
MEMORY=1024
VIDEOMEMORY=128
DISKIMAGE="/opt/vm-images/win7/win7-lab.vdi"
#NICTYPE1="virtio"
NICTYPE1="82540EM"      # Intel Pro/1000 Desktop
#CTRLTYPE="sata"
CTRLTYPE="ide"

# Create it and register it
VBoxManage createvm --name "${NAME}" --register

# Configure the hardware
VBoxManage modifyvm "${NAME}" --ostype ${OSTYPE} --memory ${MEMORY} --vram ${VIDEOMEMORY} --acpi on --ioapic on --cpus 2 --pae on --cpu-profile host --paravirtprovider default --nestedpaging on --largepages on --vtxvpid on --vtxux on --graphicscontroller vboxvga --snapshotfolder /var/tmp/vbox-${USER} --defaultfrontend sdl --nic1 nat --nictype1 ${NICTYPE1} --cableconnected1 on --audio pulse --usb on --vrde off --usb on --usbehci on

# Add the storage
VBoxManage storagectl "${NAME}" --name CTRL0 --add ${CTRLTYPE}
VBoxManage storageattach "${NAME}" --storagectl CTRL0 --port 0 --device 0 --type hdd --medium "${DISKIMAGE}" --mtype immutable --nonrotational on
