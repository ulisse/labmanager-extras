#!/bin/bash

NAME="Windows 7 snap"
OSTYPE="Windows7_64"
MEMORY=1024
VIDEOMEMORY=128
DISKIMAGE="/opt/vm-images/win7/win7-lab.vdi"
#NICTYPE1="virtio"
NICTYPE1="82540EM"      # Intel Pro/1000 Desktop
#CTRLTYPE="sata"
CTRLTYPE="ide"
SNAPNAME=shortcut
SNAPFILE="/opt/vm-images/win7/{1cdd8a9f-c5dd-45f1-9e13-0210e9fd695d}.vdi"

# Create it and register it
VBoxManage createvm --name "${NAME}" --register

# Configure the hardware
VBoxManage modifyvm "${NAME}" --ostype ${OSTYPE} --memory ${MEMORY} --vram ${VIDEOMEMORY} --acpi on --ioapic on --cpus 2 --pae on --cpu-profile host --paravirtprovider default --nestedpaging on --largepages on --vtxvpid on --vtxux on --graphicscontroller vboxvga --snapshotfolder /var/tmp/vbox-${USER} --defaultfrontend sdl --nic1 nat --nictype1 ${NICTYPE1} --cableconnected1 on --audio pulse --usb on --vrde off --usb on --usbehci on

# Add the storage
VBoxManage storagectl "${NAME}" --name CTRL0 --add ${CTRLTYPE}
VBoxManage storageattach "${NAME}" --storagectl CTRL0 --port 0 --device 0 --type hdd --medium "${DISKIMAGE}" --mtype immutable --nonrotational on

# Add a snapshot

if [ "z$SNAPFILE}" != "z" ]; then
  VBoxManage snapshot "${NAME}" take "${SNAPNAME}"

  # Read the snapshot UUID (we only have one, so we read the current)
  SNAPUUID=`VBoxManage snapshot "${NAME}" list --machinereadable | grep CurrentSnapshotUUID= | sed 's/[^=]*=//' | tr -d \"`

  # Read the config file from showvminfo
  # Also read the disk uuid from showvminfo

  # We will try to extract the filename from a line that should be like this one:
  # CTRL0 (0, 0): /var/tmp/vbox-vbconsole/{a39a1739-3815-4bd0-9cdf-0766cb7ee1e0}.vdi (UUID: a39a1739-3815-4bd0-9cdf-0766cb7ee1e0)
  SNAPLOC=`VBoxManage snapshot "${NAME}" showvminfo "${SNAPNAME}" | grep "CTRL0 (0, 0)" | sed 's/[^:]*: //' | sed 's/ .*//'`

  echo "Will try to initialize the disk snapshot"
  cp "${SNAPFILE}" "${SNAPLOC}"

  # RAM snapshot too...
fi
