#!/bin/bash

APPROVE_FILE=/local/vbconsole/bin/approve.lock
CONFIG_FILE=/local/vbconsole/bin/kiosk.settings

if [ -r ${CONFIG_FILE} ]; then
  . ${CONFIG_FILE}
fi

# Dovrebbero arrivare dall'ambiente dell'esame
#VMSETUP="gspn"
#VMNAME="GreatSPNv3.2"
#VMSETUP="win10-up"
#VMNAME="Windows 10 - UP"
#VMSETUP=""
#VMNAME="Windows 7"

/usr/bin/fluxbox &
pulseaudio --start --log-target=syslog &

while [ true ]; do
        #/usr/bin/VBoxSDL --startvm "Windows 7" --fullscreen
	if [ "z${VMSETUP}" != "z" ]; then
	  	/opt/vm-images/scripts/vm-setup "${VMSETUP}"
		/opt/vm-images/scripts/vm-start "${VMSETUP}" --no-run
		#~/bin/createvm.win7snap.sh ${VMNAME}
        fi

	/opt/vm-images/scripts/vm-start "${VMSETUP}" --no-init
        #VBoxSDL --startvm "${VMNAME}" --fullscreen

	# Wait until it is closed
	VMSTATE=`VBoxManage showvminfo "${VMNAME}" --machinereadable | grep VMState=`
	while [ "$VMSTATE" == 'VMState="running"' ]; do
		sleep 5;
		VMSTATE=`VBoxManage showvminfo "${VMNAME}" --machinereadable | grep VMState=`
	done

	#/usr/bin/xeyes -geometry 1024x768 -shape
	#/usr/bin/xterm
done
